//
//  CellData.swift
//  Meet Šabac
//
//  Created by Milan Banjanin on 07/05/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import Foundation

struct CellData {
    var opened = Bool()
    var title = String()
    var icon: String?
    var sectionData = String()
}

struct SideCellData {
    var opened = Bool()
    var title = String()
    var icon = String()
    var sectionData = [String]()
}

//
//  CalendarTableViewController.swift
//  Meet Šabac
//
//  Created by Milan Banjanin on 04/05/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import UIKit

class CalendarTableViewController: UITableViewController {

    var items: [CalendarTableViewItem] = [] {
        didSet {
            for i in 0...(items.count - 1) {
                tableViewData.append(CellData(opened: false, title: items[i].title, icon: items[i].icon, sectionData: items[i].text))
            }
        }
    }
    
    var tableViewData: [CellData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchCalendarData()
        
        let nib = UINib(nibName: "CalendarTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "CalendarTableViewCell")
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return tableViewData.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableViewData[section].opened == true {
            return 2
        } else {
            return 1
        }
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CalendarTableViewCell") as! CalendarTableViewCell
            cell.title.text = tableViewData[indexPath.section].title
            if let icon = tableViewData[indexPath.section].icon {
                cell.icon.image = UIImage(named: icon)
            }
            
            return cell
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "SectionCell") {
                cell.textLabel?.text = tableViewData[indexPath.section].sectionData
                return cell
            }
            return UITableViewCell()
        }
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableViewData[indexPath.section].opened == true {
            tableViewData[indexPath.section].opened = false
            let sections = IndexSet.init(integer: indexPath.section)
            tableView.reloadSections(sections, with: .none)
        } else {
            tableViewData[indexPath.section].opened = true
            let sections = IndexSet.init(integer: indexPath.section)
            tableView.reloadSections(sections, with: .none)
        }
        
    }
    
    override func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        if tableViewData[indexPath.section].opened == true {
            tableViewData[indexPath.section].opened = false
            let sections = IndexSet.init(integer: indexPath.section)
            tableView.reloadSections(sections, with: .none)
        } else {
            tableViewData[indexPath.section].opened = true
            let sections = IndexSet.init(integer: indexPath.section)
            tableView.reloadSections(sections, with: .none)
        }
    }

}

extension CalendarTableViewController {
    
    func fetchCalendarData() {
        
        guard let url = Bundle.main.url(forResource: "CalendarOfEvents", withExtension: "json") else {
            fatalError("Unable to find CalendarOfEvents.json")
        }
        
        guard let data = try? Data(contentsOf: url) else {
            fatalError("Unable to load CalendarOfEvents.json")
        }
        
        let decoder = JSONDecoder()
        
        guard let items = try? decoder.decode([CalendarTableViewItem].self, from: data) else {
            fatalError("Failed to decode CalendarOfEvents.json")
        }
        
        self.items = items
    }
    
}

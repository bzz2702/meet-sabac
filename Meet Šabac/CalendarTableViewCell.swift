//
//  CalendarTableViewCell.swift
//  Meet Šabac
//
//  Created by Milan Banjanin on 05/05/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import UIKit



class CalendarTableViewCell: UITableViewCell {

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
}

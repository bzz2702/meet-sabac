//
//  CalendarTableViewItem.swift
//  Meet Šabac
//
//  Created by Milan Banjanin on 05/05/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import Foundation

struct CalendarTableViewItem: Codable {
    let icon: String
    let title: String
    let text: String
}

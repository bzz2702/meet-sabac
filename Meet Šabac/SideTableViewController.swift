//
//  SideTableViewController.swift
//  Meet Šabac
//
//  Created by Milan Banjanin on 06/05/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import UIKit

protocol SideViewControllerDelegate {
    func didSelectItem(_ item: CultureGuideItem, index: Int)
}

class SideTableViewController: UITableViewController {

    
    
    var menuArray = [("comedy-92", "CULTURAL GOODS"),
                     ("comedy-92", "ARCHITECTURAL GEMS"),
                     ("comedy-92", "MONUMENTS"),
                     ("comedy-92", "INSTITUTES"),
                     ("comedy-92", "HISTORICAL SITES"),
                     ("comedy-92", "MURALS"),
                     ("comedy-92", "OTHERS")]
    
    var array: [CultureGuideItem] = [] {
        didSet {
            tableViewData.append(SideCellData(opened: false, title: menuArray[0].1, icon: menuArray[0].0, sectionData: [array[0].title, array[1].title, array[2].title, array[3].title, array[4].title, array[5].title]))

            tableViewData.append(SideCellData(opened: false, title: menuArray[1].1, icon: menuArray[1].0, sectionData: [array[6].title, array[7].title, array[8].title, array[9].title, array[10].title, array[11].title]))
            
            tableView.reloadData()
        }
    }
    
    var delegate: SideViewControllerDelegate?
    
    var tableViewData: [SideCellData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
    }
    
    // TBD: sideVC now not showing data. check 3 methods below!!!

    override func numberOfSections(in tableView: UITableView) -> Int {
         return tableViewData.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableViewData[section].opened == true {
            return tableViewData[section].sectionData.count + 1
        } else {
            return 1
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SideTableViewCell") as! SideTableViewCell
            cell.label.text = tableViewData[indexPath.section].title
            cell.icon.image = UIImage(named: tableViewData[indexPath.section].icon)
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SectionCellX") else { return UITableViewCell() }
            cell.detailTextLabel?.text = tableViewData[indexPath.section].sectionData[indexPath.row - 1]
            cell.detailTextLabel?.numberOfLines = 0
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            if tableViewData[indexPath.section].opened == true {
                tableViewData[indexPath.section].opened = false
                let sections = IndexSet.init(integer: indexPath.section)
                tableView.reloadSections(sections, with: .none)
            } else {
                tableViewData[indexPath.section].opened = true
                let sections = IndexSet.init(integer: indexPath.section)
                tableView.reloadSections(sections, with: .none)
            }
        } else {

            var index: Int!
            
            // TBD: change  when json file is complete so that numbers of rows is section match with numbers below
            switch indexPath.section {
                case 0:
                    index = indexPath.row - 1
                case 1:
                    index = indexPath.row + 6
                case 2:
                    index = indexPath.row + 12
                case 3:
                    index = indexPath.row + 18
                case 4:
                    index = indexPath.row + 24
                case 5:
                    index = indexPath.row + 30
                case 6:
                    index = indexPath.row + 36
                default:
                    break
            }
            let item = array[index]
            delegate?.didSelectItem(item, index: index)
        }

        
    }
    
}

//
//  DetailsViewController.swift
//  Meet Šabac
//
//  Created by Milan Banjanin on 04/05/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var styleLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var item: CultureGuideItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populate(with: item)
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    func populate(with item: CultureGuideItem) {
//        image.image = UIImage(named: item.image)
        title = item.title
        titleLabel.text = item.title
        styleLabel.text = "Style: \(item.style)"
        textLabel.text = item.text
        addressLabel.text = "Location: \(item.address)"
    }

}

extension DetailsViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // TBD: item.images.count (when item.image is changed to item.images)
        return item.images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! CollectionViewCell
        cell.image.image = UIImage(named: item.images[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = UIScreen.main.bounds.size.width
        let height = UIScreen.main.bounds.size.height / 2
        
        return CGSize(width: width, height: height)
    }
    
    
}

//
//  HomeViewController.swift
//  Meet Šabac
//
//  Created by Milan Banjanin on 04/05/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func cultureButtonTapped(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ContainerViewController") as! ContainerViewController
        show(vc, sender: self)
    }
    
    @IBAction func calendarButtonTapped(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CalendarTableViewController") as! CalendarTableViewController
        show(vc, sender: self)
    }
    

}


//
//  MapViewController.swift
//  Meet Šabac
//
//  Created by Milan Banjanin on 04/05/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

@objc
protocol MapViewControllerDelegate {
    @objc optional func toggleRightPanel()
    @objc optional func collapseSidePanels()
}

class MapViewController: UIViewController, CLLocationManagerDelegate {
    
    var delegate: MapViewControllerDelegate?
    
    @IBOutlet weak var mapView: MKMapView!
    
    var items: [CultureGuideItem] = []
    var annotations: [CultureObject] = []
    
    var locationManager: CLLocationManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let sabacLocation = CLLocationCoordinate2D(latitude: 44.74667, longitude: 19.69)
        mapView.region = MKCoordinateRegion(center: sabacLocation, latitudinalMeters: 5000, longitudinalMeters: 5000)
        
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestWhenInUseAuthorization()
        
        mapView.showsUserLocation = true
        mapView.delegate = self
        
        fetchCultureGuideData()
        addAnnotations()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager?.startUpdatingLocation()
        }
    }
    
    func addAnnotations() {
        for i in 0...(items.count - 1) {
            let annotation = CultureObject(title: items[i].title, coordinate: CLLocationCoordinate2D(latitude: items[i].latitude, longitude: items[i].longitude), itemInArray: i)
            
            mapView.addAnnotation(annotation)
            annotations.append(annotation)
        }
    }

}

extension MapViewController {
    
    func fetchCultureGuideData() {
        
        guard let url = Bundle.main.url(forResource: "CultureGuide", withExtension: "json") else {
            fatalError("Unable to find CultureGuide.json")
        }
        
        guard let data = try? Data(contentsOf: url) else {
            fatalError("Unable to load CultureGuide.json")
        }
        
        let decoder = JSONDecoder()
        
        guard let items = try? decoder.decode([CultureGuideItem].self, from: data) else {
            fatalError("Failed to decode CultureGuide.json")
        }
        
        self.items = items
    }
    
}

extension MapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard annotation is CultureObject else { return nil}
        
        let identifier = "CultureObject"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView!.canShowCallout = true
            
            let btn = UIButton(type: .detailDisclosure)
            annotationView!.rightCalloutAccessoryView = btn
            
        } else {
            annotationView!.annotation = annotation
        }
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DetailsViewController") as! DetailsViewController
        let cultureObject = view.annotation as! CultureObject
        vc.item = items[cultureObject.itemInArray]
        show(vc, sender: self)
        
    }
    
}

extension MapViewController: SideViewControllerDelegate {
    
    func didSelectItem(_ item: CultureGuideItem, index: Int) {
        let itemLocation = CLLocationCoordinate2D(latitude: item.latitude, longitude: item.longitude)
        mapView.region = MKCoordinateRegion(center: itemLocation, latitudinalMeters: 100, longitudinalMeters: 100)
        delegate?.collapseSidePanels?()
        
       mapView.selectAnnotation(annotations[index], animated: true)
    }
    
}

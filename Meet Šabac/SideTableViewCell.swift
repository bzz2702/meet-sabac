//
//  SideTableViewCell.swift
//  Meet Šabac
//
//  Created by Milan Banjanin on 07/05/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import UIKit

class SideTableViewCell: UITableViewCell {
    
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var label: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()

    }


}

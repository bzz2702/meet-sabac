//
//  Extension.swift
//  Meet Šabac
//
//  Created by Milan Banjanin on 06/05/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    static func mainStoryboard() -> UIStoryboard { return UIStoryboard(name: "Main", bundle: Bundle.main) }
    
    static func rightViewController() -> SideTableViewController? {
        return mainStoryboard().instantiateViewController(withIdentifier: "SideTableViewController") as? SideTableViewController
    }
    
    static func mapViewController() -> MapViewController? {
        return mainStoryboard().instantiateViewController(withIdentifier: "MapViewController") as? MapViewController
    }
}

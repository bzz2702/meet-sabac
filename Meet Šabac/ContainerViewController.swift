//
//  ContainerViewController.swift
//  Meet Šabac
//
//  Created by Milan Banjanin on 06/05/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import UIKit

class ContainerViewController: UIViewController, MapViewControllerDelegate {

    enum SlideOutState {
        case collapsed
        case rightPanelExpanded
    }
    
    var mapNavigationController: UINavigationController!
    var mapViewController: MapViewController!
    
    var currentState: SlideOutState = .collapsed {
        didSet {
            let shouldShowShadow = currentState != .collapsed
            showShadowForMapViewController(shouldShowShadow)
        }
    }

    var rightViewController: SideTableViewController?
    
    let mapPanelExpandedOffset: CGFloat = 60
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapViewController = UIStoryboard.mapViewController()
        mapViewController.delegate = self
        
        mapNavigationController = UINavigationController(rootViewController: mapViewController)
        view.addSubview(mapNavigationController.view)
        addChild(mapNavigationController)
        
        mapNavigationController.didMove(toParent: self)

        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "menu"), style: .plain, target: self, action: #selector(toggleRightPanel))
        
    }


}

extension ContainerViewController {
    
    func toggleRightPanel() {
        let notAlreadyExpanded = (currentState != .rightPanelExpanded)
        
        if notAlreadyExpanded {
            addRightPanelViewController()
        }
        
        animateRightPanel(shouldExpand: notAlreadyExpanded)

    }
    
    func collapseSidePanels() {
        
        switch currentState {
        case .rightPanelExpanded:
            toggleRightPanel()
        default:
            break
        }
    }

    
    func addRightPanelViewController() {
        
        guard rightViewController == nil else { return }
        
        if let vc = UIStoryboard.rightViewController() {
            addChildSidePanelController(vc)
            rightViewController = vc
            
            vc.array = mapViewController.items
            
//            vc.tableViewData.append(SideCellData(opened: false, title: vc.menuArray[0].1, icon: vc.menuArray[0].0, sectionData: [vc.array[0].title, vc.array[1].title, vc.array[2].title, vc.array[3].title, vc.array[4].title, vc.array[5].title]))
//            
//            vc.tableViewData.append(SideCellData(opened: false, title: vc.menuArray[1].1, icon: vc.menuArray[1].0, sectionData: [vc.array[6].title, vc.array[7].title, vc.array[8].title, vc.array[9].title, vc.array[10].title, vc.array[11].title]))
            
        }
    }
    
    func addChildSidePanelController(_ sidePanelController: SideTableViewController) {
        sidePanelController.delegate = mapViewController
        view.insertSubview(sidePanelController.view, at: 0)
        
        addChild(sidePanelController)
        sidePanelController.didMove(toParent: self)
    }


    
    func animateRightPanel(shouldExpand: Bool) {
        
        if shouldExpand {
            currentState = .rightPanelExpanded
            animateMapPanelXPosition(
                targetPosition: -mapNavigationController.view.frame.width + mapPanelExpandedOffset)
            
        } else {
            animateMapPanelXPosition(targetPosition: 0) { _ in
                self.currentState = .collapsed
                
                self.rightViewController?.view.removeFromSuperview()
                self.rightViewController = nil
            }
        }
    }
    
    func animateMapPanelXPosition(targetPosition: CGFloat, completion: ((Bool) -> Void)? = nil) {
        
        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       usingSpringWithDamping: 0.8,
                       initialSpringVelocity: 0,
                       options: .curveEaseInOut, animations: {
                        self.mapNavigationController.view.frame.origin.x = targetPosition
        }, completion: completion)
    }

 
    func showShadowForMapViewController(_ shouldShowShadow: Bool) {
        
        if shouldShowShadow {
            mapNavigationController.view.layer.shadowOpacity = 0.8
        } else {
            mapNavigationController.view.layer.shadowOpacity = 0.0
        }
    }

    
}

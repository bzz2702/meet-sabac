//
//  CultureGuideItem.swift
//  Meet Šabac
//
//  Created by Milan Banjanin on 05/05/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import Foundation
import MapKit

struct CultureGuideItem: Codable {
    let images: [String]
    let title: String
    let address: String
    let style: String
    let latitude: Double
    let longitude: Double
    let text: String
}

class CultureObject: NSObject, MKAnnotation {
    var title: String?
    var coordinate: CLLocationCoordinate2D
    var itemInArray: Int
    
    init(title: String, coordinate: CLLocationCoordinate2D, itemInArray: Int) {
        self.title = title
        self.coordinate = coordinate
        self.itemInArray = itemInArray
    }
    
}
